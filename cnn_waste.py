import os

import numpy as np

from utils.datautils import get_data_from_disk, load_model_from_disk, save_model_to_disk, \
    init_model_out_directories, data_prep_for_cnn
from utils.modelutils import make_waste_cnn_model, make_waste_transfer_lrn_InceptionV3_with_a_unfrozen_layer, \
    make_waste_transfer_lrn_InceptionV3, make_simple_cnn_model
from utils.visualutils import graph_acc_loss, graph_predict_prob

model_name = 'cnn_waste_transfer_lrn'
dataset = 'waste'

models_directory_root = os.path.join(os.getcwd(), 'models')
models_directory = os.path.join(models_directory_root, dataset)
figures_directory_save = (os.path.join(models_directory, 'figures'))
train_test_data_dir = (os.path.join(os.getcwd(), 'data'))
predict_data_dir = (os.path.join(os.getcwd(), 'predict_data'))

waste_cnn_model = None
# goes from 0 to (number_of_parts - 1); after each run increment and run again;
# also set it  to 0 when dataset is taken in full
current_batch = 0
# set to 1 when the dataset is taken in full.
number_of_parts = 4
# if retrain flag is set then load model from disk.
retrain = False

predict = False

if not predict:

    while current_batch < number_of_parts:
        figure_name = model_name + str(current_batch)
        prev_model_name = model_name + str(current_batch - 1) + '.h5'
        print(prev_model_name)
        current_model_name = model_name + str(current_batch) + '.h5'
        init_model_out_directories(models_directory_root, models_directory, figures_directory_save)

        # load dataset and save sizes
        (X_train, y_train), (X_test, y_test) = get_data_from_disk(dataset, True, train_test_data_dir,
                                                                  number_of_parts=number_of_parts,
                                                                  current_batch=current_batch)
        image_height = X_train[0].shape[0]
        image_width = X_train[0].shape[1]
        number_of_channels = X_train[0].shape[2]
        number_of_pixels = image_height * image_width
        number_of_labels = 6
        print(image_height)
        print(image_width)
        print(number_of_channels)

        # prepare dataset
        (X_train, y_train), (X_test, y_test) = data_prep_for_cnn(X_train, y_train, X_test, y_test)

        if retrain:
            waste_cnn_model = load_model_from_disk(prev_model_name, models_directory)
        else:
            input_shape = (image_height, image_width, number_of_channels)
            # waste_cnn_model = make_waste_cnn_model(input_shape)
            # waste_cnn_model = make_waste_transfer_lrn_InceptionV3()
            waste_cnn_model = make_waste_transfer_lrn_InceptionV3_with_a_unfrozen_layer(number_of_labels)
            #waste_cnn_model = make_waste_cnn_model(input_shape, number_of_labels)

        # call fit() to train the model, and save the history
        waste_cnn_history = waste_cnn_model.fit(X_train, y_train,
                                                validation_data=(X_test, y_test), epochs=50, batch_size=64,
                                                verbose=2)

        # save model
        save_model_to_disk(waste_cnn_model, current_model_name, models_directory)

        # visualize training results
        graph_acc_loss(waste_cnn_history, figures_directory_save, figure_name)
        current_batch += 1
        retrain = True

else:

    # predict
    dataset = 'waste'

    dataset, labels = get_data_from_disk(dataset, False, predict_data_dir)

    waste_cnn_model = load_model_from_disk("cnn_waste_aug_e5_17.h5", models_directory)

    dataset = data_prep_for_cnn(dataset)
    print("prediction:")
    print(waste_cnn_model.predict_classes(dataset))
    print('actual:')
    print(labels)

    predict = waste_cnn_model.predict_proba(dataset, verbose=0)
    print(np.shape(predict))
    graph_predict_prob(predict, figures_directory_save, model_name, labels)
