from keras.constraints import maxnorm
from keras.models import Sequential
from keras.layers import Dense, Conv2D, Flatten, Dropout, MaxPooling2D, BatchNormalization, GlobalAveragePooling2D
from keras import Model
from keras.applications.inception_v3 import InceptionV3
from keras.optimizers import SGD


def make_one_hidden_layer_model_mnist(input_shape):
    number_of_classes = 10

    model = Sequential()
    model.add(Dense(input_shape, activation='relu',
                    input_shape=[input_shape]))
    model.add(Dense(number_of_classes, activation='softmax'))
    model.compile(loss='categorical_crossentropy', optimizer='adam',
                  metrics=['accuracy'])
    return model


def make_simple_cnn_model(input_shape):
    number_of_classes = 6

    model = Sequential()
    model.add(Conv2D(32, (5, 5),
                     input_shape=input_shape,
                     activation='relu',
                     padding='same'))
    model.add(Flatten())
    model.add(Dense(number_of_classes, activation='softmax'))
    model.compile(loss='categorical_crossentropy',
                  optimizer='adam', metrics=['accuracy'])
    return model


def make_bigger_cnn_model(input_shape):
    number_of_classes = 10

    model = Sequential()
    model.add(Conv2D(16, (5, 5),
                     input_shape=input_shape,
                     padding='same',
                     activation='relu',
                     kernel_constraint=maxnorm(3)))
    model.add(Dropout(0.2))
    model.add(Conv2D(8, (3, 3),
                     padding='same',
                     activation='relu',
                     kernel_constraint=maxnorm(3)))
    model.add(Dropout(0.2))
    model.add(Conv2D(8, (3, 3),
                     padding='same',
                     activation='relu',
                     kernel_constraint=maxnorm(3)))
    model.add(Dropout(0.2))
    model.add(Flatten())
    model.add(Dense(number_of_classes, activation='softmax'))
    model.compile(loss='categorical_crossentropy',
                  optimizer='adam', metrics=['accuracy'])
    return model


def make_pooling_cnn_model(input_shape):
    number_of_classes = 10

    model = Sequential()
    model.add(Conv2D(32, (5, 5),
                     input_shape=input_shape,
                     padding='same',
                     activation='relu',
                     kernel_constraint=maxnorm(3)))
    model.add(Dropout(0.2))
    model.add(MaxPooling2D(pool_size=(2, 2), padding='same'))
    model.add(Conv2D(16, (3, 3),
                     padding='same',
                     activation='relu',
                     kernel_constraint=maxnorm(3)))
    model.add(Dropout(0.2))
    model.add(MaxPooling2D(pool_size=(2, 2), padding='same'))
    model.add(Flatten())
    model.add(Dense(128, activation='relu'))
    model.add(Dense(64, activation='relu'))
    model.add(Dense(number_of_classes, activation='softmax'))
    model.compile(loss='categorical_crossentropy',
                  optimizer='adam', metrics=['accuracy'])
    return model


def make_striding_cnn_model(input_shape):
    number_of_classes = 10

    model = Sequential()
    model.add(Conv2D(32, (5, 5),
                     input_shape=input_shape,
                     padding='same',
                     strides=(2, 2),
                     activation='relu',
                     kernel_constraint=maxnorm(3)))
    model.add(Dropout(0.2))
    model.add(Conv2D(16, (3, 3),
                     padding='same',
                     strides=(2, 2),
                     activation='relu',
                     kernel_constraint=maxnorm(3)))
    model.add(Dropout(0.2))
    model.add(Flatten())
    model.add(Dense(128, activation='relu'))
    model.add(Dense(64, activation='relu'))
    model.add(Dense(number_of_classes, activation='softmax'))
    model.compile(loss='categorical_crossentropy',
                  optimizer='adam', metrics=['accuracy'])
    return model


def make_waste_cnn_model(input_shape, number_of_classes):
    model = Sequential()
    model.add(Conv2D(64, (11, 11),
                     input_shape=input_shape,
                     padding='same',
                     strides=(4, 4),
                     activation='relu',
                     kernel_constraint=maxnorm(3)))
    model.add(MaxPooling2D(pool_size=(3, 3), padding='same', strides=(2, 2)))
    model.add(BatchNormalization())
    model.add(Conv2D(32, (5, 5),
                     padding='same',
                     activation='relu',
                     kernel_constraint=maxnorm(3)))
    model.add(MaxPooling2D(pool_size=(3, 3), padding='same', strides=(2, 2)))
    model.add(BatchNormalization())
    model.add(Conv2D(16, (3, 3),
                     padding='same',
                     activation='relu',
                     kernel_constraint=maxnorm(3)))
    model.add(BatchNormalization())
    model.add(Conv2D(16, (3, 3),
                     padding='same',
                     activation='relu',
                     kernel_constraint=maxnorm(3)))
    model.add(BatchNormalization())
    model.add(Conv2D(8, (3, 3),
                     padding='same',
                     activation='relu',
                     kernel_constraint=maxnorm(3)))
    model.add(MaxPooling2D(pool_size=(3, 3), padding='same', strides=(2, 2)))
    model.add(BatchNormalization())
    model.add(Flatten())
    model.add(Dense(1024, activation='relu'))
    model.add(Dropout(0.2))
    model.add(Dense(512, activation='relu'))
    model.add(Dropout(0.2))
    model.add(Dense(256, activation='relu'))
    model.add(Dropout(0.2))
    model.add(Dense(number_of_classes, activation='softmax'))
    model.compile(loss='categorical_crossentropy',
                  optimizer='adam', metrics=['accuracy'])
    model.summary()
    return model


def make_waste_transfer_lrn_InceptionV3():
    number_of_classes = 6

    base_model = InceptionV3(weights='imagenet', include_top=False)
    for layer in base_model.layers:
        layer.trainable = False

    pool_2d = GlobalAveragePooling2D(name='pool_2d')(base_model.output)
    dense = Dense(1024, name='dense', activation='relu')(pool_2d)
    predictions = Dense(number_of_classes, activation='softmax')(dense)
    model = Model(inputs=base_model.input, outputs=predictions)
    model.compile(optimizer='rmsprop',
                  loss='categorical_crossentropy',
                  metrics=['accuracy'])
    model.summary()
    return model


def make_waste_transfer_lrn_InceptionV3_with_a_unfrozen_layer(number_of_classes):
    base_model = InceptionV3(weights='imagenet', include_top=False)
    unfreeze = False
    for layer in base_model.layers:
        if unfreeze:
            layer.trainable = False
        if layer.name == 'mixed9':
            unfreeze = True

    pool_2d = GlobalAveragePooling2D(name='pool_2d')(base_model.output)
    dense_1 = Dense(1024, activation='relu')(pool_2d)
    batch_norm = BatchNormalization()(dense_1)
    dense_2 = Dense(512, activation='relu')(batch_norm)
    predictions = Dense(number_of_classes, activation='softmax')(dense_2)
    model = Model(inputs=base_model.input, outputs=predictions)
    model.compile(optimizer=SGD(lr=0.0001, momentum=0.9),
                  loss='categorical_crossentropy',
                  metrics=['accuracy'])
    model.summary()
    return model
