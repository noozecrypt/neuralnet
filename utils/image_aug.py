import Augmentor
import os

from Augmentor.Operations import Skew

data_root = 'data'
dataset = 'waste'

current_dir = os.getcwd()
root_dir = os.path.abspath(os.path.join(os.getcwd(), os.pardir))
data_root = os.path.join(root_dir, data_root)
directory = os.path.join(data_root, dataset)

p = Augmentor.Pipeline(directory)

# p.flip_top_bottom(1) # mirror invertion

# p.shear(1.0, 25, 1) # 25 degrees  shear left

# p.random_distortion(1, 10, 13, 8) # random distort the image with 10X13 grid with magnitude as 8

# p.flip_random(1.0) # either flip by x-axis or y-axis
# p.random_distortion(1, 10, 13, 8)

#p.random_brightness(1,0.7,1)
#p.random_color(1,0.5,0.5)
#p.random_contrast(1,.2,.7)

p.flip_random(1)
p.rotate180(.5)
p.skew_corner(1, 1)


# p.skew(.2, 1)
# p.skew_left_right(.25, 1)
# p.skew_tilt(.5, 1)
# p.skew_top_bottom(.25, 1)


p.process()
