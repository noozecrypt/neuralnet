from math import sqrt

import matplotlib.pyplot as plt
import keras
import os


def graph_acc_loss(history, path, title):
    accuracy = 'accuracy'
    val_accuracy = 'val_accuracy'
    if keras.__version__ == '2.1.5':
        accuracy = 'acc'
        val_accuracy = 'val_acc'
    xs = range(len(history.history[accuracy]))

    plt.figure(figsize=(10, 3))
    plt.subplot(1, 2, 1)
    plt.plot(xs, history.history[accuracy], label='train')
    plt.plot(xs, history.history[val_accuracy], label='validation')
    plt.legend(loc='lower right')
    plt.xlabel('epochs')
    plt.ylabel('accuracy')
    plt.title(title + ' accuracy')

    plt.subplot(1, 2, 2)
    plt.plot(xs, history.history['loss'], label='train')
    plt.plot(xs, history.history['val_loss'], label='validation')
    plt.legend(loc='upper right')
    plt.xlabel('epochs')
    plt.ylabel('loss')
    plt.title(title + ' loss')
    plt.savefig(os.path.join(path, title) + '.png',
                dpi=300, bbox_inches='tight')
    # plt.show()


def graph_predict_prob(proba, path, title, labels=None):
    j = proba.shape[0]
    number_of_labels = proba.shape[1]
    c = 0
    # make sure plots_per_figure is a perfect sqr
    plots_per_figure = 4
    sqrt_plots_per_figure = sqrt(plots_per_figure)
    r = plots_per_figure
    t = j % plots_per_figure
    while j > 0:
        if j == t:
            r = t
        for k in range(r):
            plt.subplot(int(sqrt_plots_per_figure), int(sqrt_plots_per_figure), k + 1)
            plt.bar(range(number_of_labels), proba[c], color='#91C0F6', align='center')
            plt.xlim(-0.5, 9.5)
            plt.ylim(0, 1)
            if labels is None:
                plt.title('plot_#' + str(c))
            else:
                plt.title(labels[c])
            c += 1
        plt.tight_layout()
        plt.savefig(os.path.join(path, title) + '_predict_' + str(c) + '.png',
                        dpi=300, bbox_inches='tight')
        plt.show()
        j -= plots_per_figure