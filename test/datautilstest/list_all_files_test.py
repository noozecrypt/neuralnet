import os

from utils.datautils import list_all_files

test_root = os.path.abspath(os.path.join(os.getcwd(), os.pardir))
test_directory = os.path.join(test_root, "fortestdir")


def build_expected_list(filenames):
    for file in filenames:
        joined = os.path.join(test_directory, file)
        yield joined


expected_filenames = ['dataset\\label1\\img031.jpg', 'dataset\\label1\\img045.jpg', 'dataset\\label1\\img332.jpg',
                      'dataset\\label1\\img394.jpg', 'dataset\\label1\\img432.jpg', 'dataset\\label1\\img445.jpg',
                      'dataset\\label1\\img678.jpg', 'dataset\\label1\\img911.jpg',
                      'dataset\\label2\\img077.jpg', 'dataset\\label2\\img098.jpg', 'dataset\\label2\\img213.jpg',
                      'dataset\\label2\\img662.jpg', 'dataset\\label2\\img688.jpg', 'dataset\\label2\\img743.jpg',
                      'dataset\\label2\\img777.jpg', 'dataset\\label2\\img812.jpg',
                      'dataset\\label3\\img413.jpg', 'dataset\\label3\\img442.jpg', 'dataset\\label3\\img456.jpg',
                      'dataset\\label3\\img457.jpg',
                      'dataset\\label4\\img012.jpg', 'dataset\\label4\\img212.jpg', 'dataset\\label4\\img215.jpg',
                      'dataset\\label4\\img380.jpg', 'dataset\\label4\\img489.jpg', 'dataset\\label4\\img634.jpg',
                      'dataset\\label4\\img654.jpg', 'dataset\\label4\\img678.jpg', 'dataset\\label4\\img711.jpg',
                      'dataset\\label4\\img721.jpg', 'dataset\\label4\\img921.jpg', 'dataset\\label4\\img999.jpg']


expected_files_list = list(build_expected_list(expected_filenames))
actual_files_list = list(list_all_files(test_directory))

assert actual_files_list == expected_files_list

print(actual_files_list)
