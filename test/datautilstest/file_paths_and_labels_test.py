import os

from utils.datautils import file_paths_and_labels

test_root = os.path.abspath(os.path.join(os.getcwd(), os.pardir))
test_directory = os.path.join(test_root, "fortestdir")


def build_expected_file_paths_and_labels_array(file_names_and_labels_list):
    dataset_directory = os.path.join(test_directory, 'dataset')

    data = []

    for (filename, label) in file_names_and_labels_list:
        filename = os.path.join(os.path.join(dataset_directory, label), filename)
        data += [(filename, label)]
    return data


file_names_and_labels_list = [('img031.jpg', 'label1'), ('img045.jpg', 'label1'), ('img332.jpg', 'label1'),
                              ('img394.jpg', 'label1'), ('img432.jpg', 'label1'), ('img445.jpg', 'label1'),
                              ('img678.jpg', 'label1'), ('img911.jpg', 'label1'),
                              ('img077.jpg', 'label2'), ('img098.jpg', 'label2'), ('img213.jpg', 'label2'),
                              ('img662.jpg', 'label2'), ('img688.jpg', 'label2'), ('img743.jpg', 'label2'),
                              ('img777.jpg', 'label2'), ('img812.jpg', 'label2'),
                              ('img413.jpg', 'label3'), ('img442.jpg', 'label3'), ('img456.jpg', 'label3'),
                              ('img457.jpg', 'label3'),
                              ('img012.jpg', 'label4'), ('img212.jpg', 'label4'), ('img215.jpg', 'label4'),
                              ('img380.jpg', 'label4'), ('img489.jpg', 'label4'), ('img634.jpg', 'label4'),
                              ('img654.jpg', 'label4'), ('img678.jpg', 'label4'), ('img711.jpg', 'label4'),
                              ('img721.jpg', 'label4'), ('img921.jpg', 'label4'), ('img999.jpg', 'label4')]

file_names_and_labels_list_divided_into_3_second_selected = \
    [('img394.png', 'label1'), ('img432.jpg', 'label1'), ('img445.jpg', 'label1'),
     ('img662.jpg', 'label2'), ('img688.jpg', 'label2'), ('img743.jpg', 'label2'),
     ('img456.jpg', 'label3'),
     ('img489.jpg', 'label4'), ('img634.jpg', 'label4'), ('img654.jpg', 'label4'),
     ('img678.jpg', 'label4')]

expected_file_paths_and_labels_list = build_expected_file_paths_and_labels_array(file_names_and_labels_list)

actual_file_paths_and_labels_list = file_paths_and_labels(os.path.join(test_directory, 'dataset'))

assert expected_file_paths_and_labels_list == actual_file_paths_and_labels_list

print(actual_file_paths_and_labels_list)

expected_file_paths_and_labels_list_divided_into_3_second_selected \
    = build_expected_file_paths_and_labels_array(file_names_and_labels_list_divided_into_3_second_selected)

actual_file_paths_and_labels_list_divided_into_3_second_selected = \
    file_paths_and_labels(os.path.join(test_directory, 'dataset'), 3, 1)

print(actual_file_paths_and_labels_list_divided_into_3_second_selected)

