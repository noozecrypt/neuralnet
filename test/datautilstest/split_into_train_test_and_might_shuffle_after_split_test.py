import numpy as np

from utils.datautils import split_into_train_test_and_might_shuffle_after_split

X = np.asarray(['A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L'])
y = np.asarray([1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12])

# test_size = 0.25 or 1/4; original size of dataset is 12 after split testing dataset's size should be 12/4 = 3
# and training dataset's size should be 12 - 3 = 9
(X_train, y_train), (X_test, y_test) = split_into_train_test_and_might_shuffle_after_split(X, y, 0.25, False)

assert X_test.shape[0] == 3
assert X_train.shape[0] == 9