import os

import numpy as np

from utils.datautils import get_data_from_disk
from utils.datautils import data_prep_for_cnn

test_root = os.path.abspath(os.path.join(os.getcwd(), os.pardir))
test_directory = os.path.join(test_root, "fortestdir")

root = os.path.abspath(os.path.join(test_root, os.pardir))
data_directory = os.path.join(root, 'data')

# returns a 2-tuple (or element array).

data = get_data_from_disk('dataset', False, test_directory)

# the elements of the 2-tuple are ndarrays, first ndarray contains 32 (as we have 32 images in total) ndarrays each
# representing an image file and the second ndarray contains 32 ndarray each representing the corresponding image file's
# label

# get the first element of the 2-tuple array
X = data[0]
# get the second element of the 2-tuple array
y = data[1]

assert X.shape[0] == 32
assert y.shape[0] == 32

print('X.shape[0] = ' + str(X.shape[0]))
print('Y.shape[0] = ' + str(y.shape[0]))

# get an ndarray from the ndarray of 32 ndarrays representing the image files and assert its structure to be equal to
# image_height * image_width * number_of_channels.
# our test image's image_height = image_width = 184 and it is an RGB image with 3 channels
assert np.shape(X[0]) == (184, 184, 3)

print(np.shape(X[0]))
print(X[0])

# get an ndarray from the ndarray of 32 ndarray representing the corresponding image labels and assert its structure
# to be (), i.e., a single element ndarray.

assert np.shape(y[0]) == ()

print(np.shape(y[0]))
print(y[0])

print('image height = ' + str(X[0].shape[0]))
print('image_width = ' + str(X[0].shape[1]))
print('number_of_channels = ' + str(X[0].shape[2]))

#
# data = get_data_from_disk('waste', False, data_directory)
#
# # the elements of the 2-tuple are ndarrays, first ndarray contains 2527 (as we have 2527 images in total) ndarrays each
# # representing an image file and the second ndarray contains 2527 ndarray each representing the corresponding
# # image file's label
#
# # get the first element of the 2-tuple array
# X = data[0]
# # get the second element of the 2-tuple array
# y = data[1]
#
# assert X.shape[0] == 2527
# assert y.shape[0] == 2527
#
# print('X.shape[0] = ' + str(X.shape[0]))
# print('Y.shape[0] = ' + str(y.shape[0]))
#
# # get an ndarray from the ndarray of 2527 ndarrays representing the image files and asserts its structure to be equal to
# # image_height * image_width * number_of_channels.
# # our test image's image_height = 384, image_width = 512 and it is a RGB image with 3 channels
# assert np.shape(X[0]) == (384, 512, 3)
#
# print(np.shape(X[0]))
# print(X[0])
#
# # get an ndarray from the ndarray of 2527 ndarray representing the corresponding image labels and assert its structure
# # to be (), i.e., a single element ndarray.
#
# assert np.shape(y[0]) == ()
#
# print(np.shape(y[0]))
# print(y)
#
# print('image height = ' + str(X[0].shape[0]))
# print('image_width = ' + str(X[0].shape[1]))
# print('number_of_channels = ' + str(X[0].shape[2]))
