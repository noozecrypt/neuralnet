import os

import numpy as np

from utils.datautils import get_data_from_disk
from utils.datautils import data_prep_for_cnn

test_root = os.path.abspath(os.path.join(os.getcwd(), os.pardir))
test_directory = os.path.join(test_root, "fortestdir")

root = os.path.abspath(os.path.join(test_root, os.pardir))
data_directory = os.path.join(root, 'data')

# returns a 2-tuple (or element array).

data = get_data_from_disk('dataset', False, test_directory)

# the elements of the 2-tuple are ndarrays, first ndarray contains 32 (as we have 32 images in total) ndarrays each
# representing an image file and the second ndarray contains 32 ndarray each representing the corresponding image file's
# label

# get the first element of the 2-tuple array
X = data[0]
# get the second element of the 2-tuple array
y = data[1]

data = data_prep_for_cnn(X, y)

# the elements of the 2-tuple are ndarrays, first ndarray is a 4D tensor whose structure is
# number_of_images * image_height * image_width * number_of_channels representing all the images after data preparation
# , in our case it is (32, 184, 184, 3)
# and  the second ndarray contains 32 ndarray, each of size 4 (as we have 4 labels)

# get the first element of the 2-tuple array
X = data[0]
# get the second element of the 2-tuple array
y = data[1]

assert np.shape(X) == (32, 184, 184, 3)
print(np.shape(X))
print(X)

assert np.shape(y) == (32, 4)
print(np.shape(y))
print(y)

print('number of images = ' + str(X.shape[0]))
print('image height = ' + str(X.shape[1]))
print('image_width = ' + str(X.shape[2]))
print('number_of_channels = ' + str(X.shape[3]))

# data = get_data_from_disk('waste', False, data_directory, number_of_parts=7)
#
# # the elements of the 2-tuple are ndarrays, first ndarray contains 363 (as we have 363 images in total) ndarrays
# # each representing an image file and the second ndarray contains 363 ndarray each representing the
# # corresponding image file's label
#
# # get the first element of the 2-tuple array
# X = data[0]
# # get the second element of the 2-tuple array
# y = data[1]
#
# data = data_prep_for_cnn(X, y)
#
# # the elements of the 2-tuple are ndarrays, first ndarray is a 4D tensor whose structure is
# # number_of_images * image_height * image_width * number_of_channels representing all the images after data preparation
# # , in our case it is (363, 384, 512, 3)
# # and  the second ndarray contains 363 ndarray, each of size 6 (as we have 6 labels)
#
# # get the first element of the 2-tuple array
# X = data[0]
# # get the second element of the 2-tuple array
# y = data[1]
#
# assert np.shape(X) == (363, 384, 512, 3)
# print(np.shape(X))
# print(X)
#
# assert np.shape(y) == (363, 6)
# print(np.shape(y))
# print(y)
#
# print('number of images = ' + str(X.shape[0]))
# print('image height = ' + str(X.shape[1]))
# print('image_width = ' + str(X.shape[2]))
# print('number_of_channels = ' + str(X.shape[3]))



