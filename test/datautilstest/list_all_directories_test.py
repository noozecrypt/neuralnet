import os

from utils.datautils import list_all_directories

test_root = os.path.abspath(os.path.join(os.getcwd(), os.pardir))
test_directory = os.path.join(test_root, "fortestdir")


def build_expected_list(dirnames):
    for dir in dirnames:
        joined = os.path.join(test_directory, dir)
        yield joined


expected_dirnames = ['dataset', 'dataset\\label1', 'dataset\\label2', 'dataset\\label3', 'dataset\\label4']

expected_dir_list = list(build_expected_list(expected_dirnames))
actual_dir_list = list(list_all_directories(test_directory))

assert actual_dir_list == expected_dir_list

print(actual_dir_list)
