import numpy as np

from utils.datautils import replace_dataset_label_with_one_hot_encoded_versions

labels = np.asarray(['label1', 'label1', 'label2', 'label2', 'label2', 'label3', 'label4', 'label4'])

expected_labels_list = '[[1. 0. 0. 0.]\n [1. 0. 0. 0.]\n [0. 1. 0. 0.]\n [0. 1. 0. 0.]\n [0. 1. 0. 0.]\n ' \
                       '[0. 0. 1. 0.]\n [0. 0. 0. 1.]\n [0. 0. 0. 1.]]'

actual_labels_list = replace_dataset_label_with_one_hot_encoded_versions(labels)

assert expected_labels_list == str(actual_labels_list)

print(actual_labels_list)
