import os

import numpy as np

from utils.datautils import get_data_from_disk
from utils.datautils import data_prep

test_root = os.path.abspath(os.path.join(os.getcwd(), os.pardir))
root = os.path.abspath(os.path.join(test_root, os.pardir))
data_directory = os.path.join(root, 'data')

# returns a 2-tuple (or element array).

data = get_data_from_disk('mnist', False, data_directory)

# the elements of the 2-tuple are ndarrays, first ndarray contains 32 (as we have 32 images in total) ndarrays each
# representing an image file and the second ndarray contains 32 ndarray each representing the corresponding image file's
# label

# get the first element of the 2-tuple array
X = data[0]
# get the second element of the 2-tuple array
y = data[1]

data = data_prep(X, y)

# the elements of the 2-tuple are ndarrays, first ndarray is a 2D array whose structure is
# number_of_images * number*pixels representing all the images after data preparation
# , in our case it is (32, 184, 184, 3)
# and  the second ndarray contains 32 ndarray, each of size 4 (as we have 4 labels)

# get the first element of the 2-tuple array
X = data[0]
# get the second element of the 2-tuple array
y = data[1]

assert np.shape(X) == (60000, (28*28))
print(np.shape(X))
print(X)

assert np.shape(y) == (60000, 10)
print(np.shape(y))
print(y)

print('number of images = ' + str(X.shape[0]))
print('number_of_pixels = ' + str(X.shape[1]))
