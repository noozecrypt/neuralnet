import os
from utils.datautils import get_data_from_disk, load_model_from_disk, save_model_to_disk, \
    init_model_out_directories, data_prep_for_cnn
from utils.modelutils import make_pooling_cnn_model
from utils.visualutils import graph_acc_loss, graph_predict_prob

model_name = 'pooling_cnn.h5'
dataset = 'mnist'

models_directory = os.path.join(os.getcwd(), 'models')
models_directory_current = os.path.join(models_directory, dataset)
figures_directory_save = (os.path.join(models_directory_current, 'figures'))
train_test_data_dir = (os.path.join(os.getcwd(), 'dataset'))
predict_data_dir = (os.path.join(os.getcwd(), 'predict_data'))

pooling_cnn_model = None
number_of_channels = 1
(orig_X_train, orig_y_train), (orig_X_test, orig_y_test) = ('', ''), ('', '')

init_model_out_directories(models_directory, models_directory_current, figures_directory_save)

try:
    pooling_cnn_model = load_model_from_disk(model_name, models_directory_current)
    print('model \'' + model_name + '\' succesfully loaded from disk')
except Exception:
    print('model \'' + model_name + '\' not found on disk; proceeding to build model and train')

if pooling_cnn_model is None:
    # load dataset and save sizes
    (X_train, y_train), (X_test, y_test) = get_data_from_disk(dataset, True, train_test_data_dir)
    image_height = X_train.shape[1]
    image_width = X_train.shape[2]
    number_of_pixels = image_height * image_width

    # save original dataset
    (orig_X_train, orig_y_train), (orig_X_test, orig_y_test) = (X_train, y_train), (X_test, y_test)

    # prepare dataset
    (X_train, y_train), (X_test, y_test) = data_prep_for_cnn(X_train, y_train, X_test, y_test)

    # make the model
    input_shape = (image_height,image_width, number_of_channels)
    pooling_cnn_model = make_pooling_cnn_model(input_shape)

    # call fit() to train the model, and save the history
    pooling_cnn_history = pooling_cnn_model.fit(X_train, y_train,
                                                validation_data=(X_test, y_test), epochs=100, batch_size=1024,
                                                verbose=2)

    # save model
    save_model_to_disk(pooling_cnn_model, model_name, models_directory_current)

    # visualize training results
    graph_acc_loss(pooling_cnn_history, figures_directory_save, model_name)

# predict
data, labels = get_data_from_disk(dataset, False, predict_data_dir)

data = data_prep_for_cnn(data)
print("prediction:")
print(pooling_cnn_model.predict_classes(data))
print('actual:')
print(labels)

predict = pooling_cnn_model.predict_proba(data, verbose=0)
graph_predict_prob(predict, figures_directory_save, model_name, labels)
