## Python - Computer Vision - CNN - Classification of Solid Waste using CNN

An Image Classification Model (CNN) to Classify Solid Waste into various Categories.

## Prepartion

Extract data.rar in project root, after extraction the project filesystem be root/data/{contents}; 
when using a RAR extractor right click on data.rar and select 'Extract here'.

## Research Paper

https://bitbucket.org/!api/2.0/snippets/noozecrypt/6qKpjR/751f3b972240ab6191358c6e44976c75d581108f/files/Classification%20of%20Solid%20Waste%20using%20CNN%20-%20Sanjay%20T.pdf