import os
from utils.datautils import get_data_from_disk, load_model_from_disk, save_model_to_disk, data_prep, \
    init_model_out_directories
from utils.modelutils import make_one_hidden_layer_model_mnist
from utils.visualutils import graph_acc_loss, graph_predict_prob

model_name = 'one_hidden_layer_model.h5'
dataset = 'mnist'

models_directory = os.path.join(os.getcwd(), 'models')
models_directory_current = os.path.join(models_directory, dataset)
figures_directory_save = (os.path.join(models_directory_current, 'figures'))
train_test_data_dir = (os.path.join(os.getcwd(), 'dataset'))
predict_data_dir = (os.path.join(os.getcwd(), 'predict_data'))

one_hidden_layer_model = None
(orig_X_train, orig_y_train), (orig_X_test, orig_y_test) = ('', ''), ('', '')

init_model_out_directories(models_directory, models_directory_current, figures_directory_save)

try:
    one_hidden_layer_model = load_model_from_disk(model_name, models_directory_current)
    print('model \'' + model_name + '\' succesfully loaded from disk')
except Exception:
    print('model \'' + model_name + '\' not found on disk; proceed to build model and train')

if one_hidden_layer_model is None:
    # load dataset and save sizes
    (X_train, y_train), (X_test, y_test) = get_data_from_disk(dataset, True, train_test_data_dir)
    image_height = X_train.shape[1]
    image_width = X_train.shape[2]
    number_of_pixels = image_height * image_width

    # save original dataset
    (orig_X_train, orig_y_train), (orig_X_test, orig_y_test) = (X_train, y_train), (X_test, y_test)

    # prepare dataset
    (X_train, y_train), (X_test, y_test) = data_prep(X_train, y_train, X_test, y_test)

    # make the model
    one_hidden_layer_model = make_one_hidden_layer_model_mnist(number_of_pixels)

    # call fit() to train the model, and save the history
    one_hidden_layer_history = one_hidden_layer_model.fit(X_train, y_train,
                                                          validation_data=(X_test, y_test), epochs=100, batch_size=1024,
                                                          verbose=2)

    # save model
    save_model_to_disk(one_hidden_layer_model, model_name, models_directory_current)

    # visualize training results
    graph_acc_loss(one_hidden_layer_history, figures_directory_save, model_name)

# predict
data, labels = get_data_from_disk(dataset, False, predict_data_dir)

data = data_prep(data)
print("prediction:")
print(one_hidden_layer_model.predict_classes(data))
print('actual:')
print(labels)

predict = one_hidden_layer_model.predict_proba(data, verbose=0)
graph_predict_prob(predict, figures_directory_save, model_name, labels)